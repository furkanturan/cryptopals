İlk önce soruda dediği gibi hamming uzaklığını bulan bir fonksiyon yazalım. Böyle bir fonksiyon kullanma nedenimiz, aynı anahtarla xorlanan değerlerin birbirine yakınlık göstermesi.

```python
hamming = lambda x,y: sum([bin(ord(x[i]) ^ ord(y[i])).count("1") for i in range(len(x))])
print(hamming("this is a test","wokka wokka!!!"))
```

**37**. Fonksiyon doğru çalışıyor. Şimdi belli uzunluktaki bir anahtar için hamming değerlerinin toplamını veren bir fonksiyon yazalım. Soruda 4 anahtar uzunluğunda bloğun, hamming değerlerinin toplamını bulup, ortalama değerin bulunması istenmiş.

```python
from itertools import combinations
hammingKeys = lambda s,keysize: sum([hamming(s1,s2) for s1,s2 in combinations([s[i:i+keysize] for i in range(0,4*keysize,keysize)],2)])*1.0/keysize
```

2-40 arası bütün uzunlukların hamming değerlerini bulup en küçük 3 değere sahip anahtarı alalım. Bunlar bizim adaylarımız olacak.

```python
from base64 import b64decode
file = b64decode(open("6.txt", "rb").read())
candidates = sorted([(i, "%.2f"%hammingKeys(file,i)) for i in range(2,41)], key = lambda x: x[1])[:3]
print(candidates)
```

**[(29, '16.48'), (5, '17.40'), (2, '18.00')]** adaylarımız bu uzunluklar olacak. Yine bir karakterle xorlanmış metin için en iyi anahtarı veren bir fonksiyon yazmamız lazım.

```python
eng_freqs = {'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339, 'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881, 'g': 0.0158610, 'h': 0.0492888, 'i': 0.0558094, 'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490, 'm': 0.0202124, 'n': 0.0564513, 'o': 0.0596302, 'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563, 's': 0.0515760, 't': 0.0729357, 'u': 0.0225134, 'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692, 'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182}
score = lambda s: sum([eng_freqs.get(l.lower(),0) for l in s])
def bestChoice(s):
  strlist = ["".join([chr(ord(i)^j) for i in s]) for j in range(256)]
  scorelist = [[chr(i), score(s)] for i,s in enumerate(strlist)]
  return max(scorelist, key = lambda x:x[-1])[0]
```

Bu fonksiyon sadece uzunluğu 1 olan anahtarlar için, fonksiyonu genişletmemiz lazım. Örneğin uzunluğu 29 olan bir anahtar için, elimizdeki stringi 29'ar artan stringlere bölüp, yeni elde ettiğimiz string sadece bir anahtarla xorlandığı için yukarıdaki fonksiyonu bu string için kullanabiliriz. Yani 0,29,58,87... inci sıradaki karakterler için k1'i, 1,30,59,88... için k2'yi, bu şekilde bütün anahtarları bulup, anahtarı k = k1 || k2 || .. şeklinde elde edebiliriz.

```python
bestChoiceKey = lambda s,keysize: "".join(map(bestChoice, ["".join(map(lambda x:s[x],j)) for j in [range(i,len(s),keysize) for i in range(keysize)]]))
```
Şimdi bu fonksiyonu kullanarak en iyi anahtar adaylarını bulalım.

```python
keyCandidates = [bestChoiceKey(file,i[0]) for i in candidates]
print(keyCandidates)
```

**['Terminator X: Bring the noise', 'non n', 'nn']**. Anahtarı bulduk. Açık bir anahtar olmasaydı, ingilizce skorundan yine doğru anahtarı bulabilirdik. Şimdi şifreli metni bu anahtarla xorlayıp açık metni bulalım.

```python
xor = lambda s,k: "".join([chr(ord(j)^ord(k[i%len(k)])) for i,j in enumerate(s)])
print(xor('Terminator X: Bring the noise'))
```
**I'm back and I'm ringin' the bell <br/>
A rockin' on the mike while the fly girls yell <br/>
In ecstasy in the back of me <br/>
...**
