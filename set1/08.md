Soruda **ECB** modunun probleminin, alınan metni hep aynı şifreli metne çevirmesi olduğu bize söylenmiş. O zaman bizde şifreli metindeki satırlarda, en fazla aynı 16'lık blok olanı bulmalıyız.

```python
chop = lambda s,size: [s[i:i+size] for i in range(0,len(s),size)]
lines = [chop("".join(map(lambda x: chr(int(x,16)), chop(i.strip(),2))), 16) for i in open("8.txt","rb")]
print("".join(lines[max([(i,len(s)-len(set(s))) for i,s in enumerate(lines)], key = lambda x: x[1])[0]]))
```
**'\xd8\x80a\x97@\xa8...\x86\xb0o\xba\x18j'**
